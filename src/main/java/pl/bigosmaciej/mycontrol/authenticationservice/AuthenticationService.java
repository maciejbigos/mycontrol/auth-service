package pl.bigosmaciej.mycontrol.authenticationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.bigosmaciej.mycontrol.authenticationservice.model.dto.LoginBody;

@SpringBootApplication
public class AuthenticationService {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationService.class, args);
	}

}
