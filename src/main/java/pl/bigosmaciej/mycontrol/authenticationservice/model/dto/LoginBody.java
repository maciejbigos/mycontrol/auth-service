package pl.bigosmaciej.mycontrol.authenticationservice.model.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record LoginBody(
        @NotBlank(message = "Email address is required")
        @Email(message = "This is not email address")
        String email,
        @NotBlank(message = "Password is required")
        String password
) {}
