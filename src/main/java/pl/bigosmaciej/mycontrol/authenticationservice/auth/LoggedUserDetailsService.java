package pl.bigosmaciej.mycontrol.authenticationservice.auth;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.bigosmaciej.mycontrol.authenticationservice.model.LoggedUserPrincipal;
import pl.bigosmaciej.mycontrol.authenticationservice.model.entity.User;
import pl.bigosmaciej.mycontrol.authenticationservice.model.repository.UserRepository;

import java.util.Optional;

@Service
public class LoggedUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    public LoggedUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> userOptional = repository.findByEmail(email);

        if (userOptional.isPresent()) {
            return new LoggedUserPrincipal(userOptional.get());
        }
        throw new UsernameNotFoundException(email);
    }
}
