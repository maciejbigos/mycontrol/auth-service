package pl.bigosmaciej.mycontrol.authenticationservice.auth;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.bigosmaciej.mycontrol.authenticationservice.model.dto.LoginBody;

@RestController
public class AuthController {

    @Autowired
    private JwtUtils jwtUtils;


    @PostMapping("/login")
    public String login(@Valid @RequestBody LoginBody loginBody) {
        return jwtUtils.generateToken(loginBody);
    }
}
