package pl.bigosmaciej.mycontrol.authenticationservice.auth;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.bigosmaciej.mycontrol.authenticationservice.model.LoggedUserPrincipal;
import io.jsonwebtoken.Jwts;
import pl.bigosmaciej.mycontrol.authenticationservice.model.dto.LoginBody;

import java.util.Date;
import java.util.HashMap;

@Service
public class JwtUtils {

    private static final String SECRET = "TOTALLY NOT IMPORTANT STRING";
    private static final long EXPIRATION_TIME = 864_000_000; //10 days for testing fun

    @Autowired
    private AuthenticationManager authenticationManager;

    public String generateToken(LoginBody loginBody) {

        LoggedUserPrincipal userPrincipal = getPrincipal(loginBody);


        HashMap<String, Object> claims = new HashMap<>();
        claims.put("roles", userPrincipal.getAuthorities());
        claims.put("id", userPrincipal.getId());
        claims.put("sub",userPrincipal.getUsername());

        return Jwts.builder()
                .setId(userPrincipal.getId())
                .setSubject(userPrincipal.getUsername())
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private LoggedUserPrincipal getPrincipal(LoginBody loginBody) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginBody.email(),loginBody.password()));
        return (LoggedUserPrincipal) authentication.getPrincipal();
    }

}
